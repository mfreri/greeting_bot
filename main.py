#!/usr/bin python3
#enconding: utf-8


'''
	This Discord bot answers to any generic greeting with a "Hola".
'''


import discord
import os
from dotenv import load_dotenv
import random


BOT_NAME = "greeting_bot"
VERSION = "0.2"
PROBABILITY = 50

# Get variables from .env file
load_dotenv()


def username(author):
	'''
	Extract the username before de '#'.
	
	RECIEVE:
	message.author output.
	
	RETURN:
	A string with the name before de '#' character.
	'''
	fullname = str(author)
	numeral = fullname.find('#')
	name = fullname[0:numeral]
	return name


def choose_yes(chance):
	'''
	Return true or false according to the posibility passed with chance.

	chance must be a value between 0 and 100
	'''
	if chance >= 100:
		return True
	elif chance <= 0:
		return False

	number = random.randrange(0, 100)
	
	if number <= chance:
		return True
	
	return False


# Words in lower case that triggers the Hello message
hello_words = ["hola", "hola.", "¡hola", "hola!", "hello", "hello.", "hello!", "holo", "holo!", "hi", "hi.", "hi!"]

# Words in lower case that triggers the Goodbye message
goodbye_words = ["adiós", "adios", "adiós!", "adios!", "adiós.", "adios.", "chau", "chau!", "chau.", "chau,", "chao", "chao.", "chaocito", "chaocito!"]


# Main
client = discord.Client()
response = ""

@client.event
async def on_ready():
	print("Logged in as {}...".format(client.user))
	print("Control+C to close.")

@client.event
async def on_message(message):
	if message.author == client.user:
		return
  
	if message.content.startswith("greeting_bot.version"):
		await message.channel.send("{} v{}.\nDeveloped by mfreri#4954.".format(BOT_NAME, VERSION))
  
	# Hello message
	if message.content.lower().split(' ', 1)[0] in hello_words:
		if choose_yes(PROBABILITY):
			response = "Hola, {}!".format(username(message.author))
		else:
			response = "Hola!"

		await message.channel.send(response)

	# Goodbye message
	if message.content.lower().split(' ', 1)[0] in goodbye_words:
		if choose_yes(PROBABILITY):
			response = "Adiós, {}!".format(username(message.author))
		else:
			response = "Adiós!"

		await message.channel.send(response)


#my_secret = os.environ['TOKEN']
client.run(os.getenv('TOKEN'))
